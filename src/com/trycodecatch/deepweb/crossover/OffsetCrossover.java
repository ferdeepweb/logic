package com.trycodecatch.deepweb.crossover;

import com.trycodecatch.deepweb.population.ArrayGenotype;

import java.util.Random;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Crossover for the Genetic Algorithm which cuts the parents' ArrayGenotypes
 * at a random location and concatenate the result to form a child genotype.
 */
public class OffsetCrossover implements ICrossover<ArrayGenotype> {

    private static final Random rand = new Random();
    private int offset;

    public OffsetCrossover(int offset) {
        this.offset = offset;
    }

    @Override
    public ArrayGenotype crossover(ArrayGenotype parent1, ArrayGenotype parent2) {
        int x = (parent1.size() - 1) / offset;

        x = (rand.nextInt(x) + 1) * offset;

        int[] firstParent = parent1.getList();
        int[] secondParent = parent2.getList();

        int[] childList = new int[firstParent.length];

        for (int i = 0; i < x; i++) {
            childList[i] = firstParent[i];
        }

        for (int i = x; i < childList.length; i++) {
            childList[i] = secondParent[i];
        }

        return new ArrayGenotype(childList);
    }
}
