package com.trycodecatch.deepweb.crossover;

import com.trycodecatch.deepweb.population.Genotype;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Crossover interface for the GeneticAlgorithm.
 */
public interface ICrossover<T extends Genotype> {
    T crossover(T parent1, T parent2);
}
