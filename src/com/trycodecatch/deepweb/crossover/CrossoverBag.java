package com.trycodecatch.deepweb.crossover;

import com.trycodecatch.deepweb.population.ArrayGenotype;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by DeepWeb on 10.4.2016..
 *
 * A collection of crossovers which behaves as a single crossover but uses a random crossover from the bag on call.
 */
public class CrossoverBag implements ICrossover<ArrayGenotype> {

    private static final Random rand = new Random();
    private List<ICrossover<ArrayGenotype>> crossovers = new ArrayList<>();

    public void addCrossover(ICrossover<ArrayGenotype> crossover) {
        crossovers.add(crossover);
    }

    @Override
    public ArrayGenotype crossover(ArrayGenotype parent1, ArrayGenotype parent2) {
        return crossovers.get(rand.nextInt(crossovers.size())).crossover(parent1, parent2);
    }
}
