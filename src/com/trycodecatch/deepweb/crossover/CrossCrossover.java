package com.trycodecatch.deepweb.crossover;

import com.trycodecatch.deepweb.population.ArrayGenotype;

import java.util.Random;

/**
 * Created by DeepWeb on 10.4.2016..
 *
 * Crossover for the Genetic Algorithm which cuts the parents' ArrayGenotypes
 * at multiple location by offset and concatenate the result to form a child genotype.
 */
public class CrossCrossover implements ICrossover<ArrayGenotype> {

    private static final Random rand = new Random();
    private int offset;

    public CrossCrossover(int offset) {
        this.offset = offset;
    }

    @Override
    public ArrayGenotype crossover(ArrayGenotype parent1, ArrayGenotype parent2) {

        int[] firstParent = parent1.getList();
        int[] secondParent = parent2.getList();

        int tOffset = rand.nextInt(firstParent.length) / offset;
        if (tOffset == 0) {
            tOffset++;
        }
        tOffset *= offset;

        int[] childList = new int[firstParent.length];

        for (int i = 0; i < childList.length; i++) {
            if((i / tOffset) % 2 == 0) {
                childList[i] = firstParent[i];
            } else {
                childList[i] = secondParent[i];
            }
        }

        return new ArrayGenotype(childList);
    }
}