package com.trycodecatch.deepweb.mutation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Problem;
import com.trycodecatch.deepweb.utils.ArrayGenotypeUtils;

public class FewShiftsMutation extends Mutation<ArrayGenotype> {

  private final Problem problem;

  private final ArrayGenotypeDecoder decoder;

  public FewShiftsMutation(double mutationChance, Problem problem, ArrayGenotypeDecoder decoder) {
    super(mutationChance);
    this.problem = problem;
    this.decoder = decoder;
  }

  @Override
  public void mutate(ArrayGenotype genotype) {
    int[] l = genotype.getList();

    int weekShifts = problem.getEmployeesPerShift() * 2 * 7;
    for (int weekStart = 0; weekStart < l.length; weekStart += weekShifts) {
      Map<Integer, Integer> numShiftsPerUser = new HashMap<>();
      List<Integer> positions = new ArrayList<>();

      for (int index = weekStart; index < weekStart + weekShifts; index++) {
        numShiftsPerUser.put(l[index], 0);
        positions.add(index);
      }

      Collections.shuffle(positions, rand);
//      for (int index = weekStart; index < weekStart + weekShifts; index++) {
      for (Integer index : positions) {
        numShiftsPerUser.put(l[index], numShiftsPerUser.get(l[index]) + 1);
      }

      for (Integer index : positions) {
//      for (int index = weekStart; index < weekStart + weekShifts; index++) {
        int day = decoder.getDayAt(index);

        int curDayPos = decoder.getPosForDay(day);
        int lastDayPos;
        if (day == 0) {
          lastDayPos = curDayPos;
        } else {
          lastDayPos = decoder.getPosForDay(day - 1);
        }
        int nxtDayPos = decoder.getPosForDay(day + 1);
        int nxt2DayPos = decoder.getPosForDay(day + 2);
        int curWeekPos = weekStart;
        int nxtWeekPos = weekStart+weekShifts;
        
        if (numShiftsPerUser.get(l[index]) > 4) {
          Set<Integer> allUsers = ArrayGenotypeUtils.getAllUsers(problem);
          Set<Integer> availableUsers =
              ArrayGenotypeUtils.filterAvailableUsers(problem, allUsers, day);
          Set<Integer> notTakenUsers =
              ArrayGenotypeUtils.filterNotTakenUsers(availableUsers, l, curDayPos, nxtDayPos);
          Set<Integer> dayRestUsers = ArrayGenotypeUtils.filterDayRestUsers(notTakenUsers, decoder,
              l, lastDayPos, curDayPos, nxtDayPos, nxt2DayPos, decoder.getShiftAt(index));
          Set<Integer> jobUsers =
              ArrayGenotypeUtils.filterJobUsers(problem, dayRestUsers, decoder.getJobAt(index));
          Set<Integer> fewShiftsUsers =
              ArrayGenotypeUtils.filterFewShiftsUsers(jobUsers, l, curWeekPos, nxtWeekPos);

          if (!fewShiftsUsers.isEmpty()) {
//            System.out.println("FEW MUTATION");
            numShiftsPerUser.put(l[index], numShiftsPerUser.get(l[index]) - 1);
            l[index] = ArrayGenotypeUtils.getRandElement(fewShiftsUsers);
            numShiftsPerUser.put(l[index], numShiftsPerUser.getOrDefault(l[index], 0) + 1);
          }
        }
      }
    }
  }

}
