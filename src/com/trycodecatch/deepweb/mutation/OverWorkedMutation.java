package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Mutation for the ER Scheduler which tries to remove employees who worked more than the allowed week rate.
 * If it encounters an over-worked employee, it tries to substitute it's schedule with a more appropriate employee.
 */
public class OverWorkedMutation extends Mutation<ArrayGenotype> {

    private final int weekShifts;
    private final int employees;
    private final Problem problem;
    private final ArrayGenotypeDecoder decoder;

    private final static int MAX_WEEK_SHIFTS = 4;

    public OverWorkedMutation(double mutationChance, Problem problem, ArrayGenotypeDecoder decoder) {
        super(mutationChance);
        weekShifts = problem.getEmployeesPerShift() * 2 * 7;
        employees = problem.employeesSize();
        this.problem = problem;
        this.decoder = decoder;
    }

    @Override
    public void mutate(ArrayGenotype genotype) {
        int[] l = genotype.getList();
        int[] e;

        int location;
        List<Integer> goodWorkers;

        for (int dayWindow = 0; dayWindow < l.length; dayWindow += weekShifts) {

            e = new int[employees];

            for(int index = dayWindow; index < dayWindow + weekShifts; index++) {
                location = l[index];
                e[location]++;
                if(e[location] > MAX_WEEK_SHIFTS) {
                    e[location]--;

                    goodWorkers = problem.getEmployeesByJob().get(decoder.getJobAt(index));

                    l[index] = goodWorkers.get(rand.nextInt(goodWorkers.size()));
                }
            }
        }
    }
}