package com.trycodecatch.deepweb.mutation;

import java.util.Set;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Problem;
import com.trycodecatch.deepweb.utils.ArrayGenotypeUtils;

public class NonWorkingDayMutation extends Mutation<ArrayGenotype> {

  private final Problem problem;

  private final ArrayGenotypeDecoder decoder;

  public NonWorkingDayMutation(double mutationChance, Problem problem,
      ArrayGenotypeDecoder decoder) {
    super(mutationChance);
    this.problem = problem;
    this.decoder = decoder;
  }

  @Override
  public void mutate(ArrayGenotype genotype) {
    int[] l = genotype.getList();

    int curWeekPos = 0;
    int nxtWeekPos = 0;
    int weekShifts = problem.getEmployeesPerShift() * 2 * 7;
    for (int index = 0; index < l.length; index++) {
      if (decoder.isNewWeek(index)) {
        curWeekPos = index;
        nxtWeekPos = index + weekShifts;
      }
      
      int user = l[index];
      int day = decoder.getDayAt(index);

      int curDayPos = decoder.getPosForDay(day);
      int lastDayPos;
      if (day == 0) {
        lastDayPos = curDayPos;
      } else {
        lastDayPos = decoder.getPosForDay(day - 1);
      }
      int nxtDayPos = decoder.getPosForDay(day + 1);
      int nxt2DayPos = decoder.getPosForDay(day + 2);

      if (!ArrayGenotypeUtils.isAvailableUser(problem, user, day)) {
        Set<Integer> allUsers = ArrayGenotypeUtils.getAllUsers(problem);
        Set<Integer> availableUsers =
            ArrayGenotypeUtils.filterAvailableUsers(problem, allUsers, day);
        Set<Integer> notTakenUsers =
            ArrayGenotypeUtils.filterNotTakenUsers(availableUsers, l, curDayPos, nxtDayPos);
        Set<Integer> dayRestUsers =
            ArrayGenotypeUtils.filterDayRestUsers(notTakenUsers, decoder, l, lastDayPos, curDayPos, nxtDayPos, nxt2DayPos, decoder.getShiftAt(index));
        Set<Integer> jobUsers = 
            ArrayGenotypeUtils.filterJobUsers(problem, dayRestUsers, decoder.getJobAt(index));
        Set<Integer> fewShiftsUsers = 
            ArrayGenotypeUtils.filterFewShiftsUsers(jobUsers, l, curDayPos, nxtDayPos);
        
        if (!fewShiftsUsers.isEmpty()) {
//          System.out.println("NON WORKING");
          l[index] = ArrayGenotypeUtils.getRandElement(fewShiftsUsers);
        }
      }
    }
  }

}
