package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Availability;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.List;

/**
 * Created by DeepWeb on 10.4.2016..
 *
 *
 */
public class RemoveNonWorkingDayMutation extends Mutation<ArrayGenotype> {

    private final Problem problem;
    private final ArrayGenotypeDecoder decoder;
    private final int dayShifts;

    public RemoveNonWorkingDayMutation(double mutationChance, Problem problem, ArrayGenotypeDecoder decoder) {
        super(mutationChance);
        this.problem = problem;
        this.decoder = decoder;
        dayShifts = problem.getEmployeesPerShift() * 2;
    }

    @Override
    public void mutate(ArrayGenotype genotype) {
        int[] l = genotype.getList();

        List<Integer> goodWorkers;

        for (int index = 0; index < l.length; index++) {
            if (problem.getEmployee(l[index]).getAvailability(index/dayShifts) != Availability.AVAILABLE) {
                goodWorkers = problem.getEmployeesByJob().get(decoder.getJobAt(index));
                l[index] = goodWorkers.get(rand.nextInt(goodWorkers.size()));
            }
        }
    }
}