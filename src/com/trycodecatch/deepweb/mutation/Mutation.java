package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.population.Genotype;

import java.util.Random;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Represents a mutation for the GeneticAlgorithm class. Each mutation object is given an mutation rate.
 * The rate can be changed at any time during the run of the algorithm.
 */
public abstract class Mutation<T extends Genotype> {

    protected static final Random rand = new Random();

    protected double mutationChance;

    public Mutation(double mutationChance) {
        this.mutationChance = mutationChance;
    }

    public double getMutationChance() {
        return mutationChance;
    }

    public void setMutationChance(double mutationChance) {
        this.mutationChance = mutationChance;
    }

    public abstract void mutate(T genotype);
}