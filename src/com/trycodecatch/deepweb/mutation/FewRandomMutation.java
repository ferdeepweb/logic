package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.population.ArrayGenotype;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Mutation for the ER Scheduler which tries to randomize the search space by swapping shifts with random employees.
 */

public class FewRandomMutation extends Mutation<ArrayGenotype>{

    private int employees;
    private static final double DIV = 0.01;

    public FewRandomMutation(double mutationChance, int employees) {
        super(mutationChance);
        this.employees = employees;
    }

    @Override
    public void mutate(ArrayGenotype genotype) {
        int[] l = genotype.getList();

        int hm = (int)(l.length * DIV);
        hm = rand.nextInt(hm);

        for (int i = 0; i < hm; i++) {
                l[rand.nextInt(l.length)] = rand.nextInt(employees);
        }
    }
}