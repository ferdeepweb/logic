package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Mutation for the ER Scheduler which tries to remove employees who are assigned to work multiple times on a single day.
 * If it encounters an over-worked employee, it tries to substitute it's schedule with a more appropriate employee.
 */
public class RemoveSameDayMutation extends Mutation<ArrayGenotype> {

    private final int dayShifts;
    private final int employees;
    private final Problem problem;
    private final ArrayGenotypeDecoder decoder;

    public RemoveSameDayMutation(double mutationChance, Problem problem, ArrayGenotypeDecoder decoder) {
        super(mutationChance);
        dayShifts = problem.getEmployeesPerShift() * 2;
        employees = problem.employeesSize();
        this.problem = problem;
        this.decoder = decoder;
    }

    @Override
    public void mutate(ArrayGenotype genotype) {
        int[] l = genotype.getList();
        int[] e;

        int location;
        List<Integer> goodWorkers;

        for (int dayWindow = 0; dayWindow < l.length; dayWindow += dayShifts / 2) {

            e = new int[employees];

            for(int index = dayWindow; index < dayWindow + dayShifts && index < l.length; index++) {
                location = l[index];
                e[location]++;
                if(e[location] > 1) {
                    e[location]--;

                    goodWorkers = problem.getEmployeesByJob().get(decoder.getJobAt(index));

                    l[index] = goodWorkers.get(rand.nextInt(goodWorkers.size()));
                }
            }
        }
    }
}