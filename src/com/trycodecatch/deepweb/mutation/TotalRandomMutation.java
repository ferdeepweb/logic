package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.population.ArrayGenotype;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Mutation for the ER Scheduler which tries to randomize the search space
 * by assigning random employees to every position with a predefined chance rate.
 */
public class TotalRandomMutation extends Mutation<ArrayGenotype> {

    private int employees;
    private static final int DIV = 500;

    public TotalRandomMutation(double mutationChance, int employees) {
        super(mutationChance);
        this.employees = employees;
    }

    @Override
    public void mutate(ArrayGenotype genotype) {
        int[] l = genotype.getList();

        for (int i = 0; i < l.length; i++) {
            if (rand.nextDouble() < 0.005) {
                l[i] = rand.nextInt(employees);
            }
        }
    }
}
