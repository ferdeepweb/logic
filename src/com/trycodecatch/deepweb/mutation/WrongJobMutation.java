package com.trycodecatch.deepweb.mutation;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.List;

/**
 * Created by DeepWeb on 10.4.2016..
 *
 * Mutation for the ER Scheduler which tries to remove employees who are assigned to a wrong work position.
 * If it encounters a wrongly assigned employee, it tries to substitute it's schedule with a more appropriate employee.
 */
public class WrongJobMutation extends Mutation<ArrayGenotype> {

    private final Problem problem;
    private final ArrayGenotypeDecoder decoder;
    private final int dayShifts;

    public WrongJobMutation(double mutationChance, Problem problem, ArrayGenotypeDecoder decoder) {
        super(mutationChance);
        this.problem = problem;
        this.decoder = decoder;
        dayShifts = problem.getEmployeesPerShift() * 2;
    }

    @Override
    public void mutate(ArrayGenotype genotype) {
        int[] l = genotype.getList();

        List<Integer> goodWorkers;

        for (int index = 0; index < l.length; index++) {
            if (!problem.getEmployee(l[index]).getJobs().contains(decoder.getJobAt(index))) {
                goodWorkers = problem.getEmployeesByJob().get(decoder.getJobAt(index));
                l[index] = goodWorkers.get(rand.nextInt(goodWorkers.size()));
            }
        }
    }
}