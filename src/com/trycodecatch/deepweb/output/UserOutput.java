package com.trycodecatch.deepweb.output;

import java.util.ArrayList;
import java.util.List;

public class UserOutput {

  private List<UserDayOutput> userDayOutputs = new ArrayList<>();

  public UserOutput(String user) {
    for (String userDay : user.split("\\s+")) {
      userDayOutputs.add(new UserDayOutput(userDay));
    }
  }

  public List<UserDayOutput> getUserDayOutputs() {
    return userDayOutputs;
  }

  public UserDayOutput getUserDayOutput(int index) {
    return userDayOutputs.get(index);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    for (UserDayOutput userDayOutput : userDayOutputs) {
      sb.append(userDayOutput.toString());
      sb.append(" ");
    }

    return sb.toString();
  }

}
