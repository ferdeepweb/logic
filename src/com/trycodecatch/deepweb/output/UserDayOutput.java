package com.trycodecatch.deepweb.output;

import com.trycodecatch.deepweb.problem.Job;
import com.trycodecatch.deepweb.problem.Shift;

public class UserDayOutput {

  private boolean working;

  private int baseID;

  private int teamID;

  private Shift shift;

  private Job job;

  public UserDayOutput(String userDay) {
    if (userDay.equals("_")) {
      working = false;
    } else {
      String[] parts = userDay.split("-");

      working = true;
      baseID = Integer.valueOf(parts[0]);
      teamID = Integer.valueOf(parts[1]);
      shift = Shift.fromString(parts[2]);
      job = Job.fromChar(parts[3].charAt(0));
    }
  }

  public boolean isWorking() {
    return working;
  }

  public int getBaseID() {
    return baseID;
  }

  public int getTeamID() {
    return teamID;
  }

  public Shift getShift() {
    return shift;
  }

  public Job getJob() {
    return job;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    
    if (!working) {
      sb.append("_");
    } else {
      sb.append(baseID);
      sb.append("-");
      sb.append(teamID);
      sb.append("-");
      sb.append(shift.getText());
      sb.append("-");
      sb.append(job.getText());
    }
    
    return sb.toString();
  }
  
}
