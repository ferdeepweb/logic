package com.trycodecatch.deepweb.output;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Output {

  List<UserOutput> userOutputs = new ArrayList<>();

  public Output(List<String> users) {
    for (String user : users) {
      userOutputs.add(new UserOutput(user));
    }
  }

  public static Output parseOutput(String file) throws IOException {
    return new Output(Files.readAllLines(Paths.get(file)));
  }

  public List<UserOutput> getUserOutputs() {
    return userOutputs;
  }
  
  public UserOutput getUserOutput(int index) {
    return userOutputs.get(index);
  }
  
  public List<UserDayOutput> getUsersByDay(int day) {
    List<UserDayOutput> usersByDay = new ArrayList<>();
    for (UserOutput user : userOutputs) {
      usersByDay.add(user.getUserDayOutput(day));
    }
    
    return usersByDay;
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    for (UserOutput userOutput : userOutputs) {
      sb.append(userOutput.toString());
      sb.append("\n");
    }

    return sb.toString();
  }

}
