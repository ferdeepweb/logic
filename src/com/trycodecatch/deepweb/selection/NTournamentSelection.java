package com.trycodecatch.deepweb.selection;

import com.trycodecatch.deepweb.population.Population;

import java.util.Random;

/**
 * Created by Andrija Milicevic.
 *
 * Selects N Genotypes from a Population and returns the fittest.
 */
public class NTournamentSelection extends Selection {
    private final int n;
    private static final Random rand = new Random();

    public NTournamentSelection(int n) {
        this.n = n;
    }

    @Override
    public int select(Population population) {
        double bestFitness;
        int bestIndex;

        double testFitness;
        int testIndex;

        int maxPopulation = population.size();

        bestIndex = Math.abs(rand.nextInt()) % maxPopulation;
        bestFitness = population.get(bestIndex).getFitness();

        for (int i = 1; i < n; i++) {
            testIndex = Math.abs(rand.nextInt()) % maxPopulation;
            testFitness = population.get(testIndex).getFitness();

            if (testFitness < bestFitness) {
                bestFitness = testFitness;
                bestIndex = testIndex;
            }
        }

        return bestIndex;
    }
}