package com.trycodecatch.deepweb.selection;

import com.trycodecatch.deepweb.population.Population;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Returns an index of a selected Genotype from a Population.
 */
public abstract class Selection {
    public abstract int select(Population population);
}
