package com.trycodecatch.deepweb.selection;


import com.trycodecatch.deepweb.population.Population;

import java.util.Random;

/**
 * Created by Andrija Milicevic.
 *
 * Uses a roulette wheel selection mechanism which gives priority to the fittest species.
 */
public class RouletteWheelSelection extends Selection {

    private static final Random rand = new Random();

    @Override
    public int select(Population population) {
        double num = rand.nextDouble();
        double sum = 0;
        double min = population.get(0).getFitness();

        for (int i = 0; i < population.size(); i++) {
            min = Math.min(min, population.get(i).getFitness());
        }

        min -= 0.1;

        for (int i = 0; i < population.size(); i++) {
            sum += 1.0 / (population.get(i).getFitness() - min);
        }

        double current = 0;

        for (int i = 0; i < population.size(); i++) {
            current += (1.0 / (population.get(i).getFitness() - min)) / sum;

            if (current > num) {
                return i;
            }
        }
        return population.size() - 1;
    }
}