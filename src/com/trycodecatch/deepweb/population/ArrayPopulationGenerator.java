package com.trycodecatch.deepweb.population;

import com.trycodecatch.deepweb.problem.Problem;

import java.util.Random;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public class ArrayPopulationGenerator implements IPopulationGenerator<ArrayGenotype>{

    private static final Random rand = new Random();
    private final Problem problem;

    public ArrayPopulationGenerator(Problem problem) {
        this.problem = problem;
    }

    @Override
    public Population generatePopulation(int populationSize) {
        Population<ArrayGenotype> population = new Population();

        int workers = problem.employeesSize();
        int shift = problem.getEmployeesPerShift();
        int days = problem.getWeeks() * 7;
        int totalJobSpots = shift * 2 * days;

        int[] genes;
        for (int i = 0; i < populationSize; i++) {

            genes = new int[totalJobSpots];

            for(int workPosition = 0; workPosition < totalJobSpots; workPosition++) {
                genes[workPosition] = rand.nextInt(workers);
            }

            population.add(new ArrayGenotype(genes));
        }

        return population;
    }
}
