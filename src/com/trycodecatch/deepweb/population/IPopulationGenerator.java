package com.trycodecatch.deepweb.population;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public interface IPopulationGenerator<T extends Genotype> {
    Population generatePopulation(int populationSize);
}
