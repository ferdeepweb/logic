package com.trycodecatch.deepweb.population;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public class ArrayGenotype extends Genotype {
    private int[] list;

    public ArrayGenotype(int[] list) {
        this.list = list;
    }

    public int[] getList() {
        return list;
    }

    public int size() {
        return list.length;
    }

    public ArrayGenotype copy() {
        int[] newList = new int[list.length];

        for (int i = 0; i < list.length; i++) {
            newList[i] = list[i];
        }

        return new ArrayGenotype(newList);
    }
}
