package com.trycodecatch.deepweb.population;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.phenotype.Shift;
import com.trycodecatch.deepweb.problem.Availability;
import com.trycodecatch.deepweb.problem.Problem;

public class GreedyPopulationGenerator implements IPopulationGenerator<ArrayGenotype> {

  private static final Random rand = new Random();

  private final Problem problem;

  private final ArrayGenotypeDecoder decoder;

  public GreedyPopulationGenerator(Problem problem) {
    this.problem = problem;
    decoder = new ArrayGenotypeDecoder(problem);
  }

  @Override
  public Population<ArrayGenotype> generatePopulation(int populationSize) {
    Population<ArrayGenotype> population = new Population<ArrayGenotype>();

    for (int i = 0; i < populationSize; i++) {
      population.add(generateGenotype());
    }

    return population;
  }

  private ArrayGenotype generateGenotype() {
    int arrayLen = getArrayLength();
    int[] genes = new int[arrayLen];

    Set<Integer> users = new HashSet<>();
    Set<Integer> lastDayNight = new HashSet<>();
    Set<Integer> newDayNight = new HashSet<>();
    Map<Integer, Integer> numShiftsPerUser = new HashMap<>();
    for (int index = 0; index < arrayLen; index++) {
      if (decoder.isNewWeek(index)) {
        for (int user = 0; user < problem.employeesSize(); ++user) {
          numShiftsPerUser.put(user, 0);
        }
      }

      if (decoder.isNewDay(index)) {
        lastDayNight = new HashSet<>(newDayNight);
        newDayNight.clear();

        for (int user = 0; user < problem.employeesSize(); ++user) {
          users.add(user);
        }
      }

      Set<Integer> jobUsers = new HashSet<>();
      for (Integer user : users) {
        if (problem.getEmployee(user).getJob(decoder.getJobAt(index))) {
          jobUsers.add(user);
        }
      }

      Set<Integer> availableUsers = new HashSet<>();
      for (Integer user : jobUsers) {
        if (problem.getEmployee(user)
            .getAvailability(decoder.getDayAt(index)) == Availability.AVAILABLE) {
          availableUsers.add(user);
        }
      }

      Set<Integer> dayRestUsers = new HashSet<>();
      for (Integer user : availableUsers) {
        if (decoder.getShiftAt(index) == Shift.DAY && lastDayNight.contains(user)) {
          continue;
        }
        dayRestUsers.add(user);
      }

      Set<Integer> fewShiftsUsers = new HashSet<>();
      // Integer minValue = Collections.min(dayRestUsers);
      for (Integer user : dayRestUsers) {
        if (numShiftsPerUser.get(user) < 4) {
          fewShiftsUsers.add(user);
        }
      }

      Integer user = null;

      if (fewShiftsUsers.isEmpty()) {
        if (dayRestUsers.isEmpty()) {
          if (availableUsers.isEmpty()) {
            if (jobUsers.isEmpty()) {
              user = getRandElement(users);
            } else {
              user = getRandElement(jobUsers);
            }
          } else {
            user = getRandElement(availableUsers);
          }
        } else {
          user = getRandElement(dayRestUsers);
        }
      } else {
        user = getRandElement(fewShiftsUsers);
      }

      if (decoder.getShiftAt(index) == Shift.NIGHT) {
        newDayNight.add(user);
      }
      numShiftsPerUser.put(user, numShiftsPerUser.get(user) + 1);
      users.remove(user);
      genes[index] = user;
    }

    return new ArrayGenotype(genes);
  }

  public Integer getRandElement(Set<Integer> set) {
    int item = rand.nextInt(set.size());

    int i = 0;
    for (Integer user : set) {
      if (i == item) {
        return user;
      }
      ++i;
    }
    return null;
  }

  public int getArrayLength() {
    return problem.getEmployeesPerShift() * 2 * problem.getDays();
  }

  public int getDayLength() {
    return problem.workingShifts();
  }

}
