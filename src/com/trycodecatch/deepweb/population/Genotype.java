package com.trycodecatch.deepweb.population;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public abstract class Genotype implements Comparable<Genotype> {
    private double fitness;

    private List<Double> errors = new ArrayList<>();

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public List<Double> getErrors() {
        return errors;
    }

    public void setErrors(List<Double> errors) {
        this.errors = errors;
    }

    @Override
    public int compareTo(Genotype genotype) {
        if (fitness < genotype.fitness) {
            return -1;
        }

        if (fitness > genotype.fitness) {
            return 1;
        }

        return 0;
    }

    public abstract Genotype copy();
}
