package com.trycodecatch.deepweb.population;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public class Population<T extends Genotype> implements Iterable<T>{
    protected List<T> genotypes;

    public Population() {
        genotypes = new ArrayList<>();
    }

    public int size() {
        return genotypes.size();
    }

    public void add(T genotype) {
        genotypes.add(genotype);
    }

    public T get(int index) {
        return genotypes.get(index);
    }

    public void set(int index, T genotype) {
        genotypes.set(index, genotype);
    }

    public void sort() {
        Collections.sort(genotypes);
    }

    @Override
    public Iterator iterator() {
        return genotypes.iterator();
    }
}
