package com.trycodecatch.deepweb;

import com.trycodecatch.deepweb.algorithm.ClonalSelectionAlgorithm;
import com.trycodecatch.deepweb.algorithm.GeneticAlgorithm;
import com.trycodecatch.deepweb.crossover.CrossCrossover;
import com.trycodecatch.deepweb.crossover.CrossoverBag;
import com.trycodecatch.deepweb.crossover.OffsetCrossover;
import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.evaluator.ArrayGenotypeEvaluator;
import com.trycodecatch.deepweb.mutation.*;
import com.trycodecatch.deepweb.phenotype.Phenotype;
import com.trycodecatch.deepweb.population.ArrayPopulationGenerator;
import com.trycodecatch.deepweb.population.GreedyPopulationGenerator;
import com.trycodecatch.deepweb.problem.Problem;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by DeepWeb on 10.4.2016..
 *
 * Multi-threaded main class used to run the multiple Genetic algorithms.
 * Assumes one command line argument, a path to the input file for the calendar.
 */
public class ParallelMain {

    private static final String OUTPUT_FILENAME = "output.txt";

    private static final Random rand = new Random();

    public static void main(String[] args) {

        if (args.length != 1) {
            throw new IllegalArgumentException("Need one command line argument!");
        }

        Problem problem = new Problem(args[0]);

        ArrayGenotypeDecoder decoder = new ArrayGenotypeDecoder(problem);

        List<Mutation> mutations = new ArrayList<>();
        mutations.add(new RemoveSameDayMutation(0.3, problem, decoder));
        mutations.add(new TotalRandomMutation(0.003, problem.employeesSize()));
        mutations.add(new FewRandomMutation(0.2, problem.employeesSize()));
        mutations.add(new RemoveNonWorkingDayMutation(0.2, problem, decoder));
        mutations.add(new OverWorkedMutation(0.5, problem, decoder));
        mutations.add(new WrongJobMutation(0.2, problem, decoder));

        CrossoverBag crossoverBag = new CrossoverBag();
        crossoverBag.addCrossover(new OffsetCrossover(problem.getEmployeesPerShift()));
        crossoverBag.addCrossover(new CrossCrossover(problem.getEmployeesPerShift()));

        Phenotype bestPhenotype = null;

        BlockingQueue<Phenotype> queue = new LinkedBlockingQueue<>();
        List<Thread> threads = new ArrayList<>();

        GeneticAlgorithm  algorithm = new GeneticAlgorithm(3000, 30, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new GreedyPopulationGenerator(problem), queue);
        GeneticAlgorithm algorithm2 = new GeneticAlgorithm(3000, 30, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new ArrayPopulationGenerator(problem), queue);

        mutations = new ArrayList<>();

        mutations.add(new RemoveSameDayMutation(rand.nextDouble(), problem, decoder));
        mutations.add(new TotalRandomMutation(rand.nextDouble()/500, problem.employeesSize()));
        mutations.add(new FewRandomMutation(rand.nextDouble(), problem.employeesSize()));
        mutations.add(new RemoveNonWorkingDayMutation(rand.nextDouble(), problem, decoder));
        mutations.add(new OverWorkedMutation(rand.nextDouble(), problem, decoder));
        mutations.add(new WrongJobMutation(rand.nextDouble(), problem, decoder));


        GeneticAlgorithm algorithm3 = new GeneticAlgorithm(3000, 30, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new ArrayPopulationGenerator(problem), queue);


        mutations = new ArrayList<>();
//        mutations.add(new RemoveSameDayMutation(0.3, problem, decoder));
        mutations.add(new TotalRandomMutation(0.3, problem.employeesSize()));
//        mutations.add(new FewRandomMutation(0.2, problem.employeesSize()));
        mutations.add(new NonWorkingDayMutation(0.2, problem, decoder));
        mutations.add(new JobMutation(0.2, problem, decoder));
        mutations.add(new FewShiftsMutation(0.4, problem, decoder));
//        mutations.add(new OverWorkedMutation(0.6, problem, decoder));

        GeneticAlgorithm  algorithm4 = new GeneticAlgorithm(3000, 30, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new GreedyPopulationGenerator(problem), queue);


        mutations = new ArrayList<>();
//        mutations.add(new RemoveSameDayMutation(0.1, problem, decoder));
        mutations.add(new TotalRandomMutation(0.3, problem.employeesSize()));
//        mutations.add(new FewRandomMutation(0.1, problem.employeesSize()));
        mutations.add(new NonWorkingDayMutation(0.2, problem, decoder));
        mutations.add(new JobMutation(0.2, problem, decoder));
        mutations.add(new FewShiftsMutation(0.4, problem, decoder));
//        mutations.add(new OverWorkedMutation(0.2, problem, decoder));

        crossoverBag.addCrossover(new OffsetCrossover(problem.getEmployeesPerShift()));
        crossoverBag.addCrossover(new CrossCrossover(problem.getEmployeesPerShift()));

        //GeneticAlgorithm  algorithm = new GeneticAlgorithm(500, 20, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new GreedyPopulationGenerator(problem), null);
        // GeneticAlgorithm algorithm = new GeneticAlgorithm(100, 30, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new ArrayPopulationGenerator(problem), null);

        ClonalSelectionAlgorithm algorithm5 = new ClonalSelectionAlgorithm(2000, 20, new ArrayGenotypeEvaluator(problem), decoder, mutations, problem, new ArrayPopulationGenerator(problem), queue);

        threads.add(new Thread(algorithm5));
        threads.add(new Thread(algorithm));
        threads.add(new Thread(algorithm2));
        threads.add(new Thread(algorithm3));
        threads.add(new Thread(algorithm4));

        for (Thread thread : threads) {
            thread.start();
        }

        int counter = threads.size();

        Phenotype testPhenotype = null;

        while (counter > 0) {
            try {
                testPhenotype = queue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

//            System.out.println(testPhenotype.getFitness());

            if (testPhenotype == GeneticAlgorithm.END_PHENOTYPE) {
                counter--;
                continue;
            }

            if (bestPhenotype == null || testPhenotype.compareTo(bestPhenotype) < 0 ) {
                bestPhenotype = testPhenotype;

                printToOutputFile(bestPhenotype);
            }
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private static void printToOutputFile(Phenotype phenotype) {

        PrintWriter writer = null;

        try {
            writer = new PrintWriter(OUTPUT_FILENAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//        System.out.println(phenotype.getFitness());
        writer.write(phenotype.toString());
        writer.close();
    }
}
