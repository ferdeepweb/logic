package com.trycodecatch.deepweb;

import com.trycodecatch.deepweb.algorithm.ClonalSelectionAlgorithm;
import com.trycodecatch.deepweb.algorithm.GeneticAlgorithm;
import com.trycodecatch.deepweb.crossover.CrossCrossover;
import com.trycodecatch.deepweb.crossover.CrossoverBag;
import com.trycodecatch.deepweb.crossover.OffsetCrossover;
import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.evaluator.ArrayGenotypeEvaluator;
import com.trycodecatch.deepweb.mutation.*;
import com.trycodecatch.deepweb.phenotype.Phenotype;
import com.trycodecatch.deepweb.population.ArrayPopulationGenerator;
import com.trycodecatch.deepweb.population.GreedyPopulationGenerator;
import com.trycodecatch.deepweb.problem.Problem;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Single-threaded main class used to run the Genetic algorithm.
 * Assumes one command line argument, a path to the input file for the calendar.
 */
public class Main {

    private static final String OUTPUT_FILENAME = "output.txt";

    public static void main(String[] args) {

        if (args.length != 1) {
            throw new IllegalArgumentException("Need one command line argument!");
        }

        Problem problem = new Problem(args[0]);

        ArrayGenotypeDecoder decoder = new ArrayGenotypeDecoder(problem);

//        List<Mutation> mutations = new ArrayList<>();
//        mutations.add(new RemoveSameDayMutation(0.3, problem, decoder));
//        mutations.add(new TotalRandomMutation(0.003, problem.employeesSize()));
//        mutations.add(new FewRandomMutation(0.2, problem.employeesSize()));
//        mutations.add(new RemoveNonWorkingDayMutation(0.2, problem, decoder));
//        mutations.add(new OverWorkedMutation(0.5, problem, decoder));
//        mutations.add(new WrongJobMutation(0.2, problem, decoder));

        List<Mutation> mutations = new ArrayList<>();
//        mutations.add(new RemoveSameDayMutation(0.1, problem, decoder));
        mutations.add(new TotalRandomMutation(0.3, problem.employeesSize()));
//        mutations.add(new FewRandomMutation(0.1, problem.employeesSize()));
        mutations.add(new NonWorkingDayMutation(0.2, problem, decoder));
        mutations.add(new JobMutation(0.2, problem, decoder));
        mutations.add(new FewShiftsMutation(0.4, problem, decoder));
//        mutations.add(new OverWorkedMutation(0.2, problem, decoder));

        CrossoverBag crossoverBag = new CrossoverBag();
        crossoverBag.addCrossover(new OffsetCrossover(problem.getEmployeesPerShift()));
        crossoverBag.addCrossover(new CrossCrossover(problem.getEmployeesPerShift()));

        //GeneticAlgorithm  algorithm = new GeneticAlgorithm(500, 20, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new GreedyPopulationGenerator(problem), null);
        // GeneticAlgorithm algorithm = new GeneticAlgorithm(100, 30, new ArrayGenotypeEvaluator(problem), decoder, mutations, crossoverBag, problem, new ArrayPopulationGenerator(problem), null);

        ClonalSelectionAlgorithm algorithm = new ClonalSelectionAlgorithm(50, 20, new ArrayGenotypeEvaluator(problem), decoder, mutations, problem, new ArrayPopulationGenerator(problem), null);
        algorithm.run();

        printToOutputFile(algorithm.getBestPhenotype());

    }

    private static void printToOutputFile(Phenotype phenotype) {

        PrintWriter writer = null;

        try {
            writer = new PrintWriter(OUTPUT_FILENAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        writer.write(phenotype.toString());
        writer.close();
    }
}
