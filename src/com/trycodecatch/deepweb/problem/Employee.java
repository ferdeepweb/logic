package com.trycodecatch.deepweb.problem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Represents a single employee at an ER with its preferences and competences.
 */
public class Employee {
    private int baseID;
    private Set<Job> jobs = new HashSet<>();
    private List<Availability> availabilities = new ArrayList<>();

    public Employee(String employee) {
        String[] parts = employee.split("\\s+");

        baseID = Integer.parseInt(parts[0]);

        for (int i = 0; i < parts[1].length(); ++i) {
            jobs.add(Job.fromChar(parts[1].charAt(i)));
        }

        for (int i = 0; i < parts[2].length(); ++i) {
            availabilities.add(Availability.fromChar(parts[2].charAt(i)));
        }
    }

    public int getBaseID() {
        return baseID;
    }

    public int availabilitySize() {
        return availabilities.size();
    }

    public Availability getAvailability(int index) {
        return availabilities.get(index);
    }

    public boolean getJob(Job job) {
        return jobs.contains(job);
    }

    public Set getJobs() {
        return jobs;
    }
}