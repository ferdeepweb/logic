package com.trycodecatch.deepweb.problem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Represents an ER team with its needed competences.
 */
public class Team implements Iterable<Job>{
    private List<Job> teammates = new ArrayList<>();

    public Team(String team) {
        for (int i = 0; i < team.length(); ++i) {
            teammates.add(Job.fromChar(team.charAt(i)));
        }
    }

    public int teamatesSize() {
        return teammates.size();
    }

    public Job getTeamate(int index) {
        return teammates.get(index);
    }

    public List<Job> getTeamates() {
        return teammates;
    }

    @Override
    public String toString() {
        String s = "";

        for (Job job : teammates) {
            s += job + " ";
        }

        return s;
    }

    @Override
    public Iterator<Job> iterator() {
        return teammates.iterator();
    }
}
