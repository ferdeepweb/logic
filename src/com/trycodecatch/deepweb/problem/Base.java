package com.trycodecatch.deepweb.problem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Represents a single ER base and contains its associated teams.
 */
public class Base implements Iterable<Team>{
    private List<Team> teams = new ArrayList<>();
    private int size;

    public Base(String allTeams) {
        for(String team : allTeams.split("\\s+")) {
            teams.add(new Team(team));
            size += team.length();
        }
    }

    public int getSize() {
        return size;
    }

    public int getTeamSize(){
        return teams.size();
    }

    public int teamsSize() {
        return teams.size();
    }

    public Team getTeam(int index) {
        return teams.get(index);
    }

    @Override
    public Iterator<Team> iterator() {
        return teams.iterator();
    }
}