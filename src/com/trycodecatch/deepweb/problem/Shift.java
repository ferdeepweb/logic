package com.trycodecatch.deepweb.problem;

/**
 * Working shift.
 * Day shift - 08:00 - 20:00
 * Night shift - 20:00 - 08:00
 */
public enum Shift {
  DAY("D"), NIGHT("N");
  
  private String text;

  Shift(String text) {
      this.text = text;
  }
  
  public String getText() {
    return this.text;
  }

  public static Shift fromString(String cShift) {
      for (Shift shift : Shift.values()) {
          if (shift.text.equals(cShift)) {
              return shift;
          }
      }
      throw new IllegalArgumentException("No constant with text " + cShift + " found.");
  }
}
