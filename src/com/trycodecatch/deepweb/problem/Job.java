package com.trycodecatch.deepweb.problem;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Enumeration of the ER jobs.
 */
public enum Job {
    DOCTOR('M'), TECHNICIAN('T'), DRIVER('V'), DISPATCHER('D');

    private char text;

    Job(char text) {
        this.text = text;
    }

    public char getText() {
        return this.text;
    }

    public static Job fromChar(char cJob) {
        for (Job job : Job.values()) {
            if (job.text == cJob) {
                return job;
            }
        }
        throw new IllegalArgumentException("No constant with text " + cJob + " found.");
    }

    public char asChar() {
        return text;
    }
}
