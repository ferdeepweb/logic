package com.trycodecatch.deepweb.problem;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Represents an employees day schedule.
 */
public enum Availability {
    AVAILABLE('D'), DAY_OFF('S'), VACATION('G'), SICK('B');

    private char text;

    Availability(char text) {
        this.text = text;
    }

    public static Availability fromChar(char cAvailability) {
        for (Availability availability : Availability.values()) {
            if (availability.text == cAvailability) {
                return availability;
            }
        }
        throw new IllegalArgumentException("No constant with text " + cAvailability + " found.");
    }
}
