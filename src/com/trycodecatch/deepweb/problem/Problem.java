package com.trycodecatch.deepweb.problem;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Specification of the ER Scheduler problem parsed from an input file.
 */
public class Problem {
    private int weeks;
    private int employeesPerShift;
    List<Base> bases = new ArrayList<>();
    List<Employee> employees = new ArrayList<>();
    Map<Job, List<Integer>> employeesByJob = new HashMap<>();

    public Problem(String filename) {
        Scanner scanner;

        try {
            scanner = new Scanner(Paths.get(filename));
        } catch (IOException e) {
            throw new IllegalArgumentException("Invalid input file...");
        }

        weeks = scanner.nextInt();
        int counter = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < counter; i++) {
            bases.add(new Base(scanner.nextLine().trim()));
            employeesPerShift += bases.get(i).getSize();
        }

        counter = scanner.nextInt();
        scanner.nextLine();

        Set<Job> tJobs;

        for (Job job : Job.values()) {
            employeesByJob.put(job, new ArrayList<>());
        }

        for (int i = 0; i < counter; i++) {
            employees.add(new Employee(scanner.nextLine()));

            tJobs = employees.get(i).getJobs();

            for (Job job : tJobs) {
                employeesByJob.get(job).add(i);
            }
        }

    }

    public int workingShifts() {
        return employeesPerShift * 2;
    }

    public Map<Job, List<Integer>> getEmployeesByJob() {
        return employeesByJob;
    }

    public int getWeeks() {
        return weeks;
    }

    public int getDays() {
        return weeks * 7;
    }

    public int baseSize(){
        return bases.size();
    }

    public Base getBase(int index) {
        return bases.get(index);
    }

    public int employeesSize() {
        return employees.size();
    }

    public Employee getEmployee(int index) {
        return employees.get(index);
    }

    public int getEmployeesPerShift() {
        return employeesPerShift;
    }
}
