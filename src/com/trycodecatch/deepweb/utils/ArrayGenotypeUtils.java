package com.trycodecatch.deepweb.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.phenotype.Shift;
import com.trycodecatch.deepweb.problem.Availability;
import com.trycodecatch.deepweb.problem.Job;
import com.trycodecatch.deepweb.problem.Problem;

public class ArrayGenotypeUtils {

  private static final Random rand = new Random();
  
  public static Set<Integer> getAllUsers(Problem problem) {
    Set<Integer> allUsers = new HashSet<>();

    for (int user = 0; user < problem.employeesSize(); ++user) {
      allUsers.add(user);
    }
    return allUsers;
  }

  public static Set<Integer> filterJobUsers(Problem problem, Set<Integer> setUsers, Job job) {
    Set<Integer> jobUsers = new HashSet<>();

    for (Integer user : setUsers) {
      if (isJobUser(problem, user, job)) {
        jobUsers.add(user);
      }
    }
    return jobUsers;
  }

  public static Set<Integer> filterAvailableUsers(Problem problem, Set<Integer> setUsers, int day) {
    Set<Integer> availableUsers = new HashSet<>();

    for (Integer user : setUsers) {
      if (isAvailableUser(problem, user, day)) {
        availableUsers.add(user);
      }
    }
    return availableUsers;
  }
  
  public static Set<Integer> filterNotTakenUsers(Set<Integer> setUsers, int[] users, int start, int end) {
    Set<Integer> notTakenUsers = new HashSet<>(setUsers);
    
//    System.out.println("DAN: " + start + " " + end);
    
    for (int i = start; i < end; ++i) {
      notTakenUsers.remove(users[i]);
    }
    
    return notTakenUsers;
  }
  
  public static Set<Integer> filterDayRestUsers(Set<Integer> setUsers, ArrayGenotypeDecoder decoder, int[] users, int startLast, int endLast, int startNext, int endNext, Shift shift) {
    Set<Integer> dayRestUsers = new HashSet<>();
    Set<Integer> lastDayNight = new HashSet<>();
    Set<Integer> nextDayDay = new HashSet<>();
    
    for (int i = startLast; i < endLast; ++i) {
      if (decoder.getShiftAt(i) == Shift.NIGHT) {
        lastDayNight.add(users[i]);
      }
    }
    
    for (int i = startNext; i < endNext; ++i) {
      if (decoder.getShiftAt(i) == Shift.DAY) {
        nextDayDay.add(users[i]);
      }
    }
    
    for (Integer user : setUsers) {
      if (shift == Shift.DAY && lastDayNight.contains(user)) {
        continue;
      }
      if (shift == Shift.NIGHT && nextDayDay.contains(user)) {
        continue;
      }
      dayRestUsers.add(user);
    }
    
    return dayRestUsers;
  }
  
  public static Set<Integer> filterFewShiftsUsers(Set<Integer> setUsers, int[] users, int start, int end) {
    Set<Integer> fewShiftsUsers = new HashSet<>();
    Map<Integer, Integer> numShiftsPerUser = new HashMap<>();
    
    for (int i = start; i < end; ++i) {
      numShiftsPerUser.put(users[i], numShiftsPerUser.getOrDefault(users[i], 0) + 1);
    }
    
    for (Integer user : setUsers) {
      if (numShiftsPerUser.getOrDefault(user, 0) < 4) {
        fewShiftsUsers.add(user);;
      }
    }
    
    return fewShiftsUsers;
  }

  public static boolean isAvailableUser(Problem problem, Integer user, int day) {
    return problem.getEmployee(user).getAvailability(day) == Availability.AVAILABLE;
  }
  
  public static boolean isJobUser(Problem problem, Integer user, Job job) {
    return problem.getEmployee(user).getJob(job);
  }
  
  public static Integer getRandElement(Set<Integer> set) {
    int item = rand.nextInt(set.size());

    int i = 0;
    for (Integer user : set) {
      if (i == item) {
        return user;
      }
      ++i;
    }
    return null;
  }
  
}
