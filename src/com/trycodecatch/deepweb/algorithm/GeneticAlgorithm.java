package com.trycodecatch.deepweb.algorithm;

import com.trycodecatch.deepweb.crossover.ICrossover;
import com.trycodecatch.deepweb.decoder.IDecoder;
import com.trycodecatch.deepweb.evaluator.IEvaluator;
import com.trycodecatch.deepweb.mutation.Mutation;
import com.trycodecatch.deepweb.phenotype.Phenotype;
import com.trycodecatch.deepweb.population.Genotype;
import com.trycodecatch.deepweb.population.IPopulationGenerator;
import com.trycodecatch.deepweb.population.Population;
import com.trycodecatch.deepweb.problem.Problem;
import com.trycodecatch.deepweb.selection.NTournamentSelection;
import com.trycodecatch.deepweb.selection.Selection;

import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

/**
 * Created by DeepWeb on 9.4.2016..
 * <p>
 * Solves an universal problem using a Genetic Algorithm.
 * The problem is specified with the Problem class with it's associated evaluator, decoder and genetic parameters.
 * The algorithm can be run both single or mutli-threaded.
 */
public class GeneticAlgorithm implements Runnable {

    public static final Phenotype END_PHENOTYPE = new Phenotype();

    private static final double ELITE_PERCENT = 0.05;
    private static final int QUEUE_SEND_RATE = 10;
    private static final double CROSSOVER_RATE = 0.9;
    private static final Random rand = new Random();

    private final int MAX_ITERATION;
    private final int MAX_POPULATION;
    private Problem problem;
    private final IEvaluator evaluator;
    private final IDecoder decoder;
    private final List<Mutation> mutations;
    private final ICrossover crossover;
    private final Selection selection = new NTournamentSelection(4);
    private final BlockingQueue<Phenotype> queue;
    private Phenotype bestPhenotype;
    private final int ELITE;

    private Population<Genotype> population;

    public GeneticAlgorithm(int MAX_ITERATION, int MAX_POPULATION, IEvaluator evaluator, IDecoder decoder, List<Mutation> mutations, ICrossover crossover, Problem problem, IPopulationGenerator generator, BlockingQueue<Phenotype> queue) {
        this.MAX_ITERATION = MAX_ITERATION;
        this.MAX_POPULATION = MAX_POPULATION;
        this.evaluator = evaluator;
        this.decoder = decoder;
        this.mutations = mutations;
        this.problem = problem;
        this.queue = queue;

        this.crossover = crossover;

        ELITE = (int) (MAX_POPULATION * ELITE_PERCENT) + 1;

        population = generator.generatePopulation(MAX_POPULATION);
        evaluatePopulation();
    }

    void evaluatePopulation() {
        for (Genotype genotype : population) {
            evaluator.evaluate(genotype);
        }


        Genotype genotype = population.get(0);
        /*
        System.out.println(genotype.getFitness());
        System.out.printf("[ ");
        for (Double error : genotype.getErrors()) {
            System.out.printf("%.0f ", error);
        }
        System.out.println("]");
        */
    }

    @Override
    public void run() {
        Population newPopulation;

        int parent1;
        int parent2;

        Genotype child;

        for (int generationCount = 0; generationCount < MAX_ITERATION; generationCount++) {

            newPopulation = new Population();

            population.sort();
            bestPhenotype = decoder.decode(population.get(0));

            if (generationCount % QUEUE_SEND_RATE == 0 && queue != null) {
                try {
                    queue.put(bestPhenotype);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < ELITE; i++) {
                newPopulation.add(population.get(i));
            }

            for (int genotypeCount = 0; genotypeCount < MAX_POPULATION - ELITE; genotypeCount++) {
                parent1 = selection.select(population);
                parent2 = selection.select(population);

                if (rand.nextDouble() < CROSSOVER_RATE) {

                    child = crossover.crossover(population.get(parent1), population.get(parent2));
                } else {
                    child = population.get(parent1).copy();
                }

                for (Mutation mutation : mutations) {
                    mutation.mutate(child);
                }

                newPopulation.add(child);
            }

            population = newPopulation;
            evaluatePopulation();
        }

        if (queue != null) {
            try {
                queue.put(END_PHENOTYPE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Phenotype getBestPhenotype() {
        return bestPhenotype;
    }
}
