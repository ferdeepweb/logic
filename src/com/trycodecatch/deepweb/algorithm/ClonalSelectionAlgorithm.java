package com.trycodecatch.deepweb.algorithm;

import com.trycodecatch.deepweb.decoder.IDecoder;
import com.trycodecatch.deepweb.evaluator.IEvaluator;
import com.trycodecatch.deepweb.mutation.Mutation;
import com.trycodecatch.deepweb.phenotype.Phenotype;
import com.trycodecatch.deepweb.population.Genotype;
import com.trycodecatch.deepweb.population.IPopulationGenerator;
import com.trycodecatch.deepweb.population.Population;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by DeepWeb on 10.4.2016..
 */
public class ClonalSelectionAlgorithm implements Runnable {

    private static final double BETA = 1.2;

    private static final double ELITE_PERCENT = 0.05;
    private static final int QUEUE_SEND_RATE = 10;

    private final int MAX_ITERATION;
    private final int MAX_POPULATION;
    private Problem problem;
    private final IEvaluator evaluator;
    private final IDecoder decoder;
    private final List<Mutation> mutations;
    private final BlockingQueue<Phenotype> queue;
    private Phenotype bestPhenotype;
    private final int ELITE;
    private final int D;
    private final IPopulationGenerator generator;

    private Population<Genotype> population;

    public ClonalSelectionAlgorithm(int MAX_ITERATION, int MAX_POPULATION, IEvaluator evaluator, IDecoder decoder, List<Mutation> mutations, Problem problem, IPopulationGenerator generator, BlockingQueue<Phenotype> queue) {
        this.MAX_ITERATION = MAX_ITERATION;
        this.MAX_POPULATION = MAX_POPULATION;
        this.evaluator = evaluator;
        this.decoder = decoder;
        this.mutations = mutations;
        this.problem = problem;
        this.queue = queue;

        ELITE = (int) (MAX_POPULATION * ELITE_PERCENT) + 1;
        D = (int) (0.1 * MAX_POPULATION);

        this.generator = generator;

        population = generator.generatePopulation(MAX_POPULATION);
        evaluatePopulation(population);
    }

    void evaluatePopulation(Population<Genotype> pop) {
        for (Genotype genotype : pop) {
            evaluator.evaluate(genotype);
        }


        Genotype genotype = population.get(0);


        /*
        System.out.println(genotype.getFitness());
        System.out.printf("[ ");
        for (Double error : genotype.getErrors()) {
            System.out.printf("%.0f ", error);
        }
        System.out.println("]");
*/

    }

    @Override
    public void run() {

        Genotype solution;
        Population<Genotype> mPopulation;

        for (int iteration = 0; iteration < MAX_ITERATION; iteration++) {
            //System.out.println(iteration);

            evaluatePopulation(population);

            population.sort();
            bestPhenotype = decoder.decode(population.get(0));

            if (iteration % QUEUE_SEND_RATE == 0 && queue != null) {
                try {
                    queue.put(bestPhenotype);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            mPopulation = new Population<>();

            int hm;

            for (int i = 0; i < MAX_POPULATION; i++) {
                hm = (int) Math.round(BETA * MAX_POPULATION / (i + 1));

                for (int j = 0; j < hm; j++) {
                    solution = population.get(i).copy();

                    for (Mutation mutation : mutations) {
                        mutation.mutate(solution);
                    }

                    mPopulation.add(solution);
                }
            }

            evaluatePopulation(mPopulation);
            mPopulation.sort();

            for (int i = 0; i < MAX_POPULATION; i++) {
                population.set(i, mPopulation.get(i));
            }

            Population popul = generator.generatePopulation(D);

            for (int i = 1; i <= D; i++) {
                population.set(MAX_POPULATION - i, popul.get(i - 1));
            }
        }

        if (queue != null) {
            try {
                queue.put(GeneticAlgorithm.END_PHENOTYPE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Phenotype getBestPhenotype() {
        return bestPhenotype;
    }
}

