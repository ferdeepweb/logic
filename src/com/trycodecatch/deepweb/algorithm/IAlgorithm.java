package com.trycodecatch.deepweb.algorithm;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Universal algorithm interface for solving a problem specified with the Problem class.
 */
public interface IAlgorithm {
    void generate(String outputFilename);
}
