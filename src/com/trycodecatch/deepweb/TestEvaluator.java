package com.trycodecatch.deepweb;

import java.io.IOException;

import com.trycodecatch.deepweb.evaluator.Evaluator;
import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.problem.Problem;

/**
 * Simple tester for the evaluator.
 * Compares the specified errors from instructions and those evaluated.
 */

public class TestEvaluator {

  public static void main(String[] args) throws IOException {
    Problem problem = new Problem("input.txt");
    Output output = Output.parseOutput("output.txt");
    
    Evaluator evaluator = new Evaluator(problem);
    
    for (Double error : evaluator.evaluate(output)) {
      System.out.println(error);
    }
  }
}
