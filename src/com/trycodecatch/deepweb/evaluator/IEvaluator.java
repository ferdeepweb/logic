package com.trycodecatch.deepweb.evaluator;

import com.trycodecatch.deepweb.population.Genotype;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Evaluates a single Genotype by adding it it's fitness value.
 */
public interface IEvaluator<T extends Genotype> {
    void evaluate(T genotype);
}
