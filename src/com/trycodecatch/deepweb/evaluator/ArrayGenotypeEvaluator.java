package com.trycodecatch.deepweb.evaluator;

import com.trycodecatch.deepweb.decoder.ArrayGenotypeDecoder;
import com.trycodecatch.deepweb.decoder.IDecoder;
import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Evaluates an ArrayGenotype and gives it a corresponding fitness and error values.
 * This implementation mainly focuses on the hard constraints of the ER Scheduler problem.
 */
public class ArrayGenotypeEvaluator implements IEvaluator<ArrayGenotype> {

    private Evaluator evaluator;

    private IDecoder decoder;

    public ArrayGenotypeEvaluator(Problem problem) {
        this.evaluator = new Evaluator(problem);
        this.decoder = new ArrayGenotypeDecoder(problem);
    }

    @Override
    public void evaluate(ArrayGenotype genotype) {
        List<String> outputs = new ArrayList<>();
        for (String line : decoder.decode(genotype).toString().split("\\n")) {
            outputs.add(line);
        }

        Output output = new Output(outputs);

        List<Double> errors = evaluator.evaluate(output);

        double error = 0;
        for (int i = 0; i < 5; ++i) {
            error += errors.get(i);
        }
        genotype.setErrors(errors);
        genotype.setFitness(error);
    }
}
