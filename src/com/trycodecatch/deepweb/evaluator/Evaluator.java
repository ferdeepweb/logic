package com.trycodecatch.deepweb.evaluator;

import java.util.ArrayList;
import java.util.List;

import com.trycodecatch.deepweb.evaluator.rules.IRuleEvaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule1Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule2Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule3Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule4Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule5Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule6Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule7Evaluator;
import com.trycodecatch.deepweb.evaluator.rules.Rule8Evaluator;
import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.problem.Problem;

/**
 * Container for the evaluator rules.
 * Also evaluates every associated rule and returns a list containing the results for each evaluated rule.
 */
public class Evaluator {

  private List<IRuleEvaluator> evaluators = new ArrayList<>();

  public Evaluator(Problem problem) {
    evaluators.add(new Rule1Evaluator(problem));
    evaluators.add(new Rule2Evaluator(problem));
    evaluators.add(new Rule3Evaluator(problem));
    evaluators.add(new Rule4Evaluator(problem));
    evaluators.add(new Rule5Evaluator(problem));
    evaluators.add(new Rule6Evaluator(problem));
    evaluators.add(new Rule7Evaluator(problem));
    evaluators.add(new Rule8Evaluator(problem));
  }

  public List<Double> evaluate(Output output) {
    List<Double> errors = new ArrayList<>();

    for (IRuleEvaluator evaluator : evaluators) {
      errors.add(evaluator.evaluate(output));
    }

    return errors;
  }

}
