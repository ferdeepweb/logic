package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.problem.Availability;
import com.trycodecatch.deepweb.problem.Problem;

public class Rule1Evaluator extends AbstractRuleEvaluator {

    public Rule1Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;
        for (int user = 0; user < problem.employeesSize(); ++user) {
            for (int day = 0; day < problem.getDays(); ++day) {
                if (output.getUserOutput(user).getUserDayOutput(day).isWorking()
                        && problem.getEmployee(user).getAvailability(day) != Availability.AVAILABLE) {
                    cnt += 1;
                }
            }
        }

        return cnt;
    }

}
