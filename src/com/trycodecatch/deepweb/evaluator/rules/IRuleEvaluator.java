package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;

/**
 * Evaluates a single rule from the Output structure.
 */
public interface IRuleEvaluator {

    double evaluate(Output output);
}
