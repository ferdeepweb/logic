package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.output.UserOutput;
import com.trycodecatch.deepweb.problem.Problem;
import com.trycodecatch.deepweb.problem.Shift;

public class Rule4Evaluator extends AbstractRuleEvaluator {

    public Rule4Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;

        for (int userID = 0; userID < problem.employeesSize(); ++userID) {
            UserOutput user = output.getUserOutput(userID);
            for (int day = 1; day < problem.getDays(); ++day) {
                UserDayOutput userDayLast = user.getUserDayOutput(day - 1);
                UserDayOutput userDay = user.getUserDayOutput(day);

                if (userDayLast.isWorking() && userDay.isWorking() && userDayLast.getShift() == Shift.NIGHT
                        && userDay.getShift() == Shift.DAY) {
                    cnt += 1;
                }
            }
        }

        return cnt;
    }

}
