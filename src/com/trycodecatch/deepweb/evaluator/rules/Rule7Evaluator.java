package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.output.UserOutput;
import com.trycodecatch.deepweb.problem.Employee;
import com.trycodecatch.deepweb.problem.Problem;

public class Rule7Evaluator extends AbstractRuleEvaluator {

    public Rule7Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;

        for (int userID = 0; userID < problem.employeesSize(); ++userID) {
            UserOutput user = output.getUserOutput(userID);
            Employee employee = problem.getEmployee(userID);
            for (int day = 0; day < problem.getDays(); ++day) {
                UserDayOutput userDay = user.getUserDayOutput(day);

                if (userDay.isWorking() && userDay.getBaseID() != employee.getBaseID()) {
                    cnt += 1;
                }
            }
        }

        return cnt;
    }

}
