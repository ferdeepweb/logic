package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.output.UserOutput;
import com.trycodecatch.deepweb.problem.Problem;

public class Rule5Evaluator extends AbstractRuleEvaluator {

    public Rule5Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;

        for (int userID = 0; userID < problem.employeesSize(); ++userID) {
            UserOutput user = output.getUserOutput(userID);

            int shifts = 0;
            for (int day = 0; day < problem.getDays(); ++day) {
                if (day % 7 == 0) {
                    cnt += Math.max(shifts - 4, 0);
                    shifts = 0;
                }

                UserDayOutput userDay = user.getUserDayOutput(day);

                if (userDay.isWorking()) {
                    ++shifts;
                }
            }
            cnt += Math.max(shifts - 4, 0);
        }

        return cnt;
    }

}
