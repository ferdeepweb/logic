package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.output.UserOutput;
import com.trycodecatch.deepweb.problem.Problem;
import com.trycodecatch.deepweb.problem.Shift;

public class Rule6Evaluator extends AbstractRuleEvaluator {

    public Rule6Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;

        for (int userID = 0; userID < problem.employeesSize(); ++userID) {
            UserOutput user = output.getUserOutput(userID);

            for (int day = 0; day < problem.getDays(); ++day) {
                UserDayOutput userDay = user.getUserDayOutput(day);

                if (userDay.isWorking() && userDay.getShift() == Shift.DAY) {
                    int currentDay = day + 1;
                    if (currentDay < problem.getDays()) {
                        UserDayOutput tmpDay = user.getUserDayOutput(currentDay);
                        if (!tmpDay.isWorking() || tmpDay.getShift() != Shift.NIGHT) {
                            cnt += 1;
                            continue;
                        }
                    }
                    ++currentDay;
                    if (currentDay < problem.getDays()) {
                        UserDayOutput tmpDay = user.getUserDayOutput(currentDay);
                        if (tmpDay.isWorking()) {
                            cnt += 1;
                            continue;
                        }
                    }
                    ++currentDay;
                    if (currentDay < problem.getDays()) {
                        UserDayOutput tmpDay = user.getUserDayOutput(currentDay);
                        if (tmpDay.isWorking()) {
                            cnt += 1;
                            continue;
                        }
                    }
                }
            }
        }

        return cnt;
    }


}
