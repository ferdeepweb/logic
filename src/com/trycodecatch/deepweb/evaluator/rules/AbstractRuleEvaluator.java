package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.problem.Problem;

/**
 * Evaluates a single rule from a set of rules used by a multi valued evaluator.
 */
public abstract class AbstractRuleEvaluator implements IRuleEvaluator {

    protected Problem problem;

    public AbstractRuleEvaluator(Problem problem) {
        this.problem = problem;
    }
}
