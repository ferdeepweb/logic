package com.trycodecatch.deepweb.evaluator.rules;

import java.util.ArrayList;
import java.util.List;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.output.UserOutput;
import com.trycodecatch.deepweb.problem.Problem;

public class Rule8Evaluator extends AbstractRuleEvaluator {

    public Rule8Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        List<Integer> hoursByUser = new ArrayList<>();
        double sumHours = 0;
        for (int userID = 0; userID < problem.employeesSize(); ++userID) {
            UserOutput user = output.getUserOutput(userID);
            int hours = 0;
            for (int day = 0; day < problem.getDays(); ++day) {
                UserDayOutput userDay = user.getUserDayOutput(day);

                if (userDay.isWorking()) {
                    hours += 12;
                }
            }

            sumHours += hours;
            hoursByUser.add(hours);
        }

        double mean = sumHours / problem.employeesSize();

        double sumDev = 0;
        for (Integer hours : hoursByUser) {
            sumDev += Math.pow(hours - mean, 2);
        }

        return Math.sqrt(sumDev / (problem.employeesSize() - 1));
    }

}
