package com.trycodecatch.deepweb.evaluator.rules;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.output.UserOutput;
import com.trycodecatch.deepweb.problem.Problem;

public class Rule3Evaluator extends AbstractRuleEvaluator {

    public Rule3Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;

        for (int userID = 0; userID < problem.employeesSize(); ++userID) {
            UserOutput user = output.getUserOutput(userID);
            for (int day = 0; day < problem.getDays(); ++day) {
                UserDayOutput userDay = user.getUserDayOutput(day);

                if (userDay.isWorking()
                        && !problem.getEmployee(userID).getJobs().contains(userDay.getJob())) {
                    cnt += 1;
                }
            }
        }

        return cnt;
    }

}
