package com.trycodecatch.deepweb.evaluator.rules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.trycodecatch.deepweb.output.Output;
import com.trycodecatch.deepweb.output.UserDayOutput;
import com.trycodecatch.deepweb.problem.Base;
import com.trycodecatch.deepweb.problem.Job;
import com.trycodecatch.deepweb.problem.Problem;
import com.trycodecatch.deepweb.problem.Shift;

public class Rule2Evaluator extends AbstractRuleEvaluator {

    public Rule2Evaluator(Problem problem) {
        super(problem);
    }

    @Override
    public double evaluate(Output output) {
        double cnt = 0;
        for (int day = 0; day < problem.getDays(); ++day) {
            for (Shift shift : Shift.values()) {
                for (int baseID = 0; baseID < problem.baseSize(); ++baseID) {
                    Base base = problem.getBase(baseID);
                    for (int teamID = 0; teamID < base.teamsSize(); ++teamID) {
                        List<Job> jobs = new ArrayList<>(base.getTeam(teamID).getTeamates());

                        List<Job> oJobs = new ArrayList<>();
                        for (UserDayOutput user : output.getUsersByDay(day)) {
                            if (user.getShift() != shift || user.getBaseID() != baseID
                                    || user.getTeamID() != teamID) {
                                continue;
                            }

                            if (user.isWorking()) {
                                oJobs.add(user.getJob());
                            }
                        }

                        if (jobs.size() != oJobs.size()) {
                            cnt += 1;
                            continue;
                        }

                        Collections.sort(jobs);
                        Collections.sort(oJobs);
                        if (!jobs.equals(oJobs)) {
                            cnt += 1;
                        }
                    }
                }
            }
        }

        return cnt;
    }

}
