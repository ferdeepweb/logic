package com.trycodecatch.deepweb;

import com.trycodecatch.deepweb.problem.Base;
import com.trycodecatch.deepweb.problem.Employee;
import com.trycodecatch.deepweb.problem.Job;
import com.trycodecatch.deepweb.problem.Problem;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Simple test file for parsing the input file to the Problem class.
 * Prints the Problem to the standard output.
 */
public class TestInput {
    public static void main(String[] args) {
        Problem problem = new Problem("input.txt");

        System.out.println(problem.getWeeks());

        System.out.println(problem.baseSize());

        for (int i = 0; i < problem.baseSize(); i++) {
            Base b = problem.getBase(i);

            for (int j = 0; j < b.teamsSize(); j++) {
                System.out.println(b.getTeam(j));
            }
        }

        System.out.println(problem.employeesSize());

        for (int i = 0; i < problem.employeesSize(); i++) {

            Employee e = problem.getEmployee(i);

            System.out.println(e.getBaseID() + " " + e.getJob(Job.TECHNICIAN));

            String t = "";

            for (int j = 0; j < e.availabilitySize(); j++) {
                t += e.getAvailability(j).toString().charAt(0);
            }

            System.out.println(t);
        }
    }
}
