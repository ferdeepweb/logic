package com.trycodecatch.deepweb.phenotype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public class Phenotype implements Comparable<Phenotype>{
    public EmployeeSchedule[] employeeScheduleList;

    public Phenotype() {
    }

    public Phenotype(EmployeeSchedule[] employeeScheduleList) {
        this.employeeScheduleList = employeeScheduleList;
    }

    private double fitness;

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        boolean first;

        for (EmployeeSchedule schedule : employeeScheduleList) {

            first = true;

            EmployeeDay[] days = schedule.getDays();

            for (int i = 0; i < days.length; i++) {
                if (first) {
                    first = false;
                } else {
                    builder.append(' ');
                }

                builder.append(days[i]);
            }

            builder.append("\n");
        }

        return builder.toString();
    }

    @Override
    public int compareTo(Phenotype p) {
        if (fitness < p.fitness) {
            return -1;
        }

        if (fitness > p.fitness) {
            return 1;
        }

        return 0;
    }
}
