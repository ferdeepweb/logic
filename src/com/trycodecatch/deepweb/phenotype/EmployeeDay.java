package com.trycodecatch.deepweb.phenotype;

import com.trycodecatch.deepweb.problem.Job;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public class EmployeeDay {
    private final int baseID;
    private final int teamID;
    private final Shift shift;
    private final Job job;

    public EmployeeDay(int baseID, int teamID, Shift shift, Job job) {
        this.baseID = baseID;
        this.teamID = teamID;
        this.shift = shift;
        this.job = job;
    }

    @Override
    public String toString() {
        if (shift == Shift.NONE) {
            return "_";
        }

        return baseID + "-" + teamID + "-" + shift.asChar() + "-" + job.asChar();
    }
}
