package com.trycodecatch.deepweb.phenotype;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public class EmployeeSchedule {
    private EmployeeDay[] days;

    public EmployeeSchedule(EmployeeDay[] days) {
        this.days = days;
    }

    public EmployeeDay[] getDays() {
        return days;
    }
}
