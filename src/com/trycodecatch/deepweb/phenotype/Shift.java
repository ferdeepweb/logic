package com.trycodecatch.deepweb.phenotype;

/**
 * Created by DeepWeb on 9.4.2016..
 */
public enum Shift {
    DAY('D'), NIGHT('N'), NONE('_');

    public char asChar() {
        return asChar;
    }

    private final char asChar;

    Shift(char asChar) {
        this.asChar = asChar;
    }
    
}
