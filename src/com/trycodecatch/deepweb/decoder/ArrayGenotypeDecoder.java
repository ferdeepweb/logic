package com.trycodecatch.deepweb.decoder;

import com.trycodecatch.deepweb.phenotype.EmployeeDay;
import com.trycodecatch.deepweb.phenotype.EmployeeSchedule;
import com.trycodecatch.deepweb.phenotype.Phenotype;
import com.trycodecatch.deepweb.phenotype.Shift;
import com.trycodecatch.deepweb.population.ArrayGenotype;
import com.trycodecatch.deepweb.problem.Base;
import com.trycodecatch.deepweb.problem.Job;
import com.trycodecatch.deepweb.problem.Problem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Decodes an ArrayGenotype object to a Phenotype which is used to generate a calendar.
 * The decoder knows how to interpret ArrayGenotype array locations.
 */
public class ArrayGenotypeDecoder implements IDecoder<ArrayGenotype> {

    private final Problem problem;
    private static final EmployeeDay FREE_DAY = new EmployeeDay(0, 0, Shift.NONE, null);

    private List<EmployeeDay> eDay = new ArrayList<>();

    private List<Integer> dayAt = new ArrayList<>();

    private List<Job> jobAt = new ArrayList<>();

    private List<Shift> shiftAt = new ArrayList<>();

    private Map<Integer, Integer> dayToPos = new HashMap<>();

    public ArrayGenotypeDecoder(Problem problem) {
        this.problem = problem;

        Base base;
        int days = problem.getWeeks() * 7;
        for (int day = 0; day < days; day++) {
            for (int baseID = 0, bases = problem.baseSize(); baseID < bases; baseID++) {
                base = problem.getBase(baseID);

                for (int teamID = 0, maxTeam = base.getTeamSize(); teamID < maxTeam; teamID++) {
                    for (Job job : base.getTeam(teamID)) {
                        eDay.add(new EmployeeDay(baseID, teamID, Shift.DAY, job));
                        if (dayToPos.get(day) == null) {
                            dayToPos.put(day, dayAt.size());
                        }
                        dayAt.add(day);
                        jobAt.add(job);
                        shiftAt.add(Shift.DAY);
                    }
                    for (Job job : base.getTeam(teamID)) {
                        eDay.add(new EmployeeDay(baseID, teamID, Shift.NIGHT, job));
                        dayAt.add(day);
                        jobAt.add(job);
                        shiftAt.add(Shift.NIGHT);
                    }
                }
            }
        }
    }

    public boolean isNewWeek(int index) {
        if (index == 0) {
            return true;
        }
        return getDayAt(index) == 0 && getDayAt(index - 1) == 6;
    }

    public boolean isNewDay(int index) {
        if (index == 0) {
            return true;
        }
        return getDayAt(index) != getDayAt(index - 1);
    }

    public Integer getDayAt(int index) {
        return dayAt.get(index);
    }

    public Job getJobAt(int index) {
        return jobAt.get(index);
    }

    public Shift getShiftAt(int index) {
        return shiftAt.get(index);
    }

    public int getPosForDay(int day) {
        if (dayToPos.get(day) == null) {
            return jobAt.size();
        }
        return dayToPos.get(day);
    }

    @Override
    public Phenotype decode(ArrayGenotype genotype) {
        int days = problem.getWeeks() * 7;
        int workers = problem.employeesSize();

        EmployeeDay[][] employeeDays = new EmployeeDay[workers][days];

        for (int i = 0; i < workers; i++) {
            for (int j = 0; j < days; j++) {
                employeeDays[i][j] = FREE_DAY;
            }
        }

        int[] list = genotype.getList();

        EmployeeDay day;
        int shift = problem.getEmployeesPerShift() * 2;

        for (int i = 0; i < list.length; i++) {
            day = eDay.get(i);
            employeeDays[list[i]][i / shift] = day;
        }

        EmployeeSchedule[] schedules = new EmployeeSchedule[workers];

        for (int i = 0; i < schedules.length; i++) {
            schedules[i] = new EmployeeSchedule(employeeDays[i]);
        }

        Phenotype phenotype = new Phenotype(schedules);
        phenotype.setFitness(genotype.getFitness());

        return phenotype;
    }
}
