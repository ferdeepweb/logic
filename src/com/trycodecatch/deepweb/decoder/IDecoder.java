package com.trycodecatch.deepweb.decoder;

import com.trycodecatch.deepweb.phenotype.Phenotype;
import com.trycodecatch.deepweb.population.Genotype;

/**
 * Created by DeepWeb on 9.4.2016..
 *
 * Decoder associated with a specific Genotype representation used to decode a Genotype to a Phenotype.
 */
public interface IDecoder<T extends Genotype> {
    Phenotype decode(T genotype);
}
