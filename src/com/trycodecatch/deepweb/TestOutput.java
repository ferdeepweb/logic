package com.trycodecatch.deepweb;

import java.io.IOException;

import com.trycodecatch.deepweb.output.Output;

/**
 * Parses an output file to the Output class an then prints it to the command line.
 */

public class TestOutput {

  public static void main(String[] args) throws IOException {
    Output output = Output.parseOutput("output.txt");

    System.out.println(output.toString());
  }
}
